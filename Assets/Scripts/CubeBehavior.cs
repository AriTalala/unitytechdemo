﻿using System.Collections;
using System.IO;


using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Runtime.Serialization.Formatters.Binary;

/*
 This is Behavior code for CubeOne.
 This could be used also for other gameobjects.

in Update() we start iterating List<MovementData> and on each iteration we set coordinates to which the GameObject
ought to move. When destination has been reached, the iteration proceeds one more step and new coordinates are set.

The movement stops when we have reached the end, in this case when the third and last coordination has been reached and
we have come to the end of the List.
 */

public class CubeBehavior : MonoBehaviour
{


    List<MovementData> list = new List<MovementData>();
    int movementIteration;

    void Start()
    {

        Debug.Log("Cube has started now");

        list = Dataholder.instance.getList();
        Debug.Log("Limit is " + list.Count);

        movementIteration = 0;

    }


    void iterateMovementList() {

       
            movementIteration = movementIteration + 1;
        
        

    }

    // Update is called once per frame
    void Update()
    {

        if (movementIteration < list.Count) {

            Vector3 dest;
            float speed;

            Debug.Log("movementIteration is " + movementIteration);
            Debug.Log("limit is " + list.Count);

            if (movementIteration >= list.Count) {
                Debug.Log("OVERBOARD");
            }

            dest.x = list[movementIteration].X;
            dest.y = list[movementIteration].Y;
            dest.z = list[movementIteration].Z;
            speed = list[movementIteration].Speed;

            if (transform.position != dest)
            {
                Debug.Log("Moving");
                transform.position = Vector3.MoveTowards(transform.position, dest, Time.deltaTime * speed);

            }

            else if (transform.position == dest)
            {
                Debug.Log("Gotta iterate");
                iterateMovementList();
            }

        }
        
        
        
        
        }



    }


