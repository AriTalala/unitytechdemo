﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

/*
 This class is used to store coordinates to which a GameObject ought to move to at given speed.
 */

[System.Serializable]
public class MovementData
{

    protected float x;
    protected float y;
    protected float z;
    protected float speed;


    public MovementData() { }

    public float X {

        get { return x; }
        set { x = value; }
    }

    public float Y {

        get { return y; }
        set { y = value; }
    }

    public float Z {

        get { return z; }
        set { z = value; }

            }

    public float Speed {

        get { return speed; }
        set { speed = value; }

    }

}


