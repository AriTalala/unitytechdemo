﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

using UnityEditor;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System;

/*
 This is used to initialize MovemementData if movs.dat file aint there or load it straight of if it is.
 In either case we set a List of MovementData to a Dataholder singleton from which it can be accessed by CubeBehavior.


For starters we have three instances of movement data.
 
 */


public class GameInitScript
{
    static string path = "Assets/Scripts/charmovements.txt";
    static string binaryPath = "Assets/Scripts/movs.dat";

    static List<MovementData> movementList = new List<MovementData>();

    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad()
    {

       
                Debug.Log("init started");
       
        if (File.Exists(binaryPath))
            loadBinary();
        else
            initBinaryFile();


    }

    static void initBinaryFile()
    {

        List<MovementData> dataset = new List<MovementData>();

        MovementData dataOne = new MovementData();
        dataOne.X = 5.0f;
        dataOne.Y = 2.0f;
        dataOne.Z = 0.0f;
        dataOne.Speed = 1.75f;

        MovementData dataTwo = new MovementData();
        dataTwo.X = 5.0f;
        dataTwo.Y = 6.0f;
        dataTwo.Z = 7.0f;
        dataTwo.Speed = 1.75f;

        MovementData dataThree = new MovementData();
        dataThree.X = 0.0f;
        dataThree.Y = 13.0f;
        dataThree.Z = 3.0f;
        dataThree.Speed = 1.75f;

        dataset.Add(dataOne);
        dataset.Add(dataTwo);
        dataset.Add(dataThree);


        FileStream dataStream = new FileStream(binaryPath, FileMode.Create);
        BinaryFormatter converter = new BinaryFormatter();
        converter.Serialize(dataStream, dataset);
        dataStream.Close();

        Debug.Log("initialized Binary");

        Dataholder.instance.setList(dataset);


    }

    static void loadBinary()
    {

        MovementData data = new MovementData();
        List<MovementData> list;

        Debug.Log("loaded Binary");
        FileStream fs = new FileStream(binaryPath, FileMode.Open);
        BinaryFormatter bf = new BinaryFormatter();
        list = (List <MovementData>) bf.Deserialize(fs);
        data = list[0];
        fs.Close();

        Debug.Log("X = " + data.X);
        Debug.Log("Y = " + data.Y);

        movementList = list;

        Dataholder.instance.setList(movementList);
        
        
        
    }


  




}
