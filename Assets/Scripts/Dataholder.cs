﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 This class is used as a singleton to store MovementData for GameObjects.
 Certain changes ought to be made if this is to persist between Scenes however..
 
 */

public class Dataholder
{

    private static Dataholder _instance;

    List<MovementData> movementList = new List<MovementData>();


    public static Dataholder instance
    {

        get {
            if (_instance == null)
                _instance = new Dataholder();
            return _instance;

        }
    }

    public void setList(List<MovementData> list) {

        movementList = list;

    }

    public List<MovementData> getList() { return movementList; }


    

}
